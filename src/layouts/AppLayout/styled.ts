import styled from 'styled-components';

import { container } from 'components/AppTheme/GlobalStyled';
import BodyBg from 'assets/img/body-bg.jpg';

export const Layout = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  &::before {
    content: '';
    position: fixed;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    background: center/cover no-repeat url(${BodyBg});
    z-index: -1;
  }
`;

export const Container = styled.div`
  ${container};
`;
