import { Route, Routes } from 'react-router-dom';

import { GamesListPage } from 'pages/GamesListPage';
import { GameDetailsPage } from 'pages/GameDetailsPage';
import { NotFoundPage } from 'pages/NotFoundPage';
import { AppHeader } from 'components/AppHeader';
import { Footer } from 'components/AppFooter';
import { Layout, Container } from './styled';

export const AppLayout = () => {
  return (
    <Layout>
      <AppHeader />
      <Container>
        <Routes>
          <Route
            path="/"
            element={<GamesListPage />}
          />
          <Route
            path="/game/:gameId"
            element={<GameDetailsPage />}
          />
          <Route
            path="*"
            element={<NotFoundPage />}
          />
        </Routes>
      </Container>
      <Footer />
    </Layout>
  );
};
