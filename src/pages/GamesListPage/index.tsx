import { FiltersGames } from './components/FiltersGames';
import { ListOfGames } from './components/ListOfGames';

export const GamesListPage = () => {
  return (
    <>
      <FiltersGames />
      <ListOfGames />
    </>
  );
};
