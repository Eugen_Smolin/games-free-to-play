import { RootState } from 'store/index';

export const selectors = {
  list: (state: RootState) => state.gamesListReducer.list,
  params: (state: RootState) => state.gamesListReducer.params,
  pag: (state: RootState) => state.gamesListReducer.pag,
  error: (state: RootState) => state.gamesListReducer.error,
  fetchingStatus: (state: RootState) => state.gamesListReducer.fetchingStatus,
};
