import { StatusEnum } from 'types/enum';
import { TPagination } from 'types/libs';

export type TLocalCardGame = {
  id: number;
  developer: string;
  freetogameProfileUrl: string;
  gameUrl: string;
  genre: string;
  platform: string;
  publisher: string;
  releaseDate: string;
  shortDescription: string;
  thumbnail: string;
  title: string;
};

export type TApiCardGame = {
  id: number;
  developer: string;
  freetogame_profile_url: string;
  game_url: string;
  genre: string;
  platform: string;
  publisher: string;
  release_date: string;
  short_description: string;
  thumbnail: string;
  title: string;
};

export type TLocalParamsGames = {
  platform: string;
  category: string;
  sortBy: string;
};

export type TApiParamsGames = {
  platform: string;
  category: string;
  'sort-by': string;
};

export type TGamesSlice = {
  list: TLocalCardGame[][];
  params: TLocalParamsGames;
  pag: TPagination;
  error: {
    status: number;
    message: string;
  };
  fetchingStatus: StatusEnum;
};

export type TAxiosReturnError = {
  status: number;
  message: string;
};

export type TAxiosDataError = {
  status: number;
  status_message: string;
};

export type TThunkAPI = {
  rejectValue: TAxiosReturnError;
  state: { gamesListReducer: TGamesSlice };
};
