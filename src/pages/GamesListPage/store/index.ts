import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { selectors } from './selectors';
import { thunks } from './thunks';
import { StatusEnum } from 'types/enum';
import { TPagination } from 'types/libs';
import { GameModel } from 'pages/GamesListPage/models';
import { arrayChunks } from 'utils/prepareArray';
import { TGamesSlice, TLocalCardGame, TLocalParamsGames } from './types';

const initialState: TGamesSlice = {
  list: [],
  params: {
    platform: 'all',
    category: 'all',
    sortBy: 'relevance',
  },
  pag: {
    page: 1,
    perPage: 12,
    total: 0,
  },
  error: {
    status: 404,
    message: '',
  },
  fetchingStatus: StatusEnum.IDLE,
};

const slice = createSlice({
  name: 'games',
  initialState,
  reducers: {
    SET_PARAMS: (
      state,
      { payload }: PayloadAction<Partial<TLocalParamsGames>>
    ) => {
      state.params = { ...state.params, ...payload };
    },
    SET_PAGINATION: (
      state,
      { payload }: PayloadAction<Partial<TPagination>>
    ) => {
      state.pag = { ...state.pag, ...payload };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getListOfGamesThunk.pending, (state) => {
        state.fetchingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getListOfGamesThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          const chunkArr = payload.map((game) => GameModel(game));
          state.list = arrayChunks<TLocalCardGame>(chunkArr, state.pag.perPage);
          state.pag.total = chunkArr.length;
        }
        state.fetchingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getListOfGamesThunk.rejected, (state, { payload }) => {
        if (payload) state.error = payload;
        state.fetchingStatus = StatusEnum.FAIL;
      });
  },
});

export const gamesListSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export const gamesListReducer = slice.reducer;
