import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';

import { http } from 'api/http';
import { WEB_API_ROUTES } from 'api/api-routes';
import { extractAllFromObject } from 'utils/prepareObject';
import { GamesParamsModel } from 'pages/GamesListPage/models';
import {
  TApiCardGame,
  TApiParamsGames,
  TAxiosDataError,
  TThunkAPI,
} from './types';

const getListOfGamesThunk = createAsyncThunk<
  TApiCardGame[],
  undefined,
  TThunkAPI
>(
  'gamesListReducer/getListOfGamesThunk',
  async (_, { getState, rejectWithValue }) => {
    const { params } = getState().gamesListReducer;
    const apiParams = GamesParamsModel(params);
    const extractParams = extractAllFromObject<TApiParamsGames>(apiParams);

    try {
      const { data } = await http.get(WEB_API_ROUTES.getListOfGames, {
        params: extractParams,
      });
      return data;
    } catch (err) {
      if (axios.isAxiosError(err)) {
        const { status, data } = err.response as AxiosResponse;
        const dataError = data as TAxiosDataError;
        return rejectWithValue({ status, message: dataError.status_message });
      }

      throw err;
    }
  }
);

export const thunks = {
  getListOfGamesThunk,
};
