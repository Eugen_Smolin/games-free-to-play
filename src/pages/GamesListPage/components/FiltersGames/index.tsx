import { useState } from 'react';

import { RBox } from 'ui/RBox';
import { RSelect } from 'ui/RSelect';
import { FilterSwitcher } from 'pages/GamesListPage/components/FilterSwitcher';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { gamesListSlice } from 'pages/GamesListPage/store';
import { platformList, categoryList, sortByList } from './config';
import { Wrap, Row } from './styled';

export const FiltersGames = () => {
  const [isVisible, setVisible] = useState(false);
  const dispatch = useAppDispatch();
  const { platform, category, sortBy } = useAppSelector(
    gamesListSlice.selectors.params
  );

  const handleSwitcher = () => {
    setVisible((prev) => !prev);
  };

  const onChangeSelect = (property: string) => (value: string) => {
    dispatch(gamesListSlice.actions.SET_PARAMS({ [property]: value }));
    dispatch(gamesListSlice.actions.SET_PAGINATION({ page: 1 }));
    dispatch(gamesListSlice.thunks.getListOfGamesThunk());
  };

  return (
    <Wrap>
      <RBox>
        <FilterSwitcher onClick={handleSwitcher} />
        {isVisible && (
          <Row>
            <RSelect
              value={platform}
              onChange={onChangeSelect('platform')}
              options={platformList}
            />
            <RSelect
              value={category}
              onChange={onChangeSelect('category')}
              options={categoryList}
            />
            <RSelect
              value={sortBy}
              onChange={onChangeSelect('sortBy')}
              options={sortByList}
            />
          </Row>
        )}
      </RBox>
    </Wrap>
  );
};
