import styled from 'styled-components';

export const Wrap = styled.div`
  margin-bottom: 24px;

  @media (max-width: 500px) {
    max-width: 380px;
    margin: 0 auto 24px;
  }
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 16px;
  padding-top: 24px;

  @media (max-width: 800px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 500px) {
    grid-template-columns: 1fr;
  }
`;
