import { useEffect } from 'react';

import { RLoader } from 'ui/RLoader';
import { RPagination } from 'ui/RPagination';
import { ErrorServer } from 'components/ErrorServer';
import { CardGame } from 'pages/GamesListPage/components/CardGame';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { StatusEnum } from 'types/enum';
import { gamesListSlice } from 'pages/GamesListPage/store';
import { Typography } from 'components/AppTheme/GlobalStyled/components';
import { Wrap, List } from './styled';

export const ListOfGames = () => {
  const dispatch = useAppDispatch();
  const listOfGames = useAppSelector(gamesListSlice.selectors.list);
  const listPag = useAppSelector(gamesListSlice.selectors.pag);
  const listStatus = useAppSelector(gamesListSlice.selectors.fetchingStatus);
  const error = useAppSelector(gamesListSlice.selectors.error);

  useEffect(() => {
    if (!listOfGames.length) {
      dispatch(gamesListSlice.thunks.getListOfGamesThunk());
    }
  }, []);

  const onChangePage = (page: number) => {
    dispatch(gamesListSlice.actions.SET_PAGINATION({ page: page }));
  };

  if (listStatus === StatusEnum.PENDING) {
    return <RLoader />;
  }

  if (listStatus === StatusEnum.FAIL) {
    return (
      <ErrorServer
        status={error.status}
        message={error.message}
      />
    );
  }

  if (!listOfGames.length) {
    return (
      <Typography
        fz="20px"
        fw={600}
        textPos="center"
      >
        List is empty
      </Typography>
    );
  }

  return (
    <Wrap>
      <List>
        {listOfGames[listPag.page - 1].map((game) => (
          <CardGame
            key={game.id}
            game={game}
          />
        ))}
      </List>
      <RPagination
        page={listPag.page}
        perPage={listPag.perPage}
        total={listPag.total}
        onChangePage={onChangePage}
      />
    </Wrap>
  );
};
