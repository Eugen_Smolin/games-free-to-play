import { FC } from 'react';

import FilterUrl from 'assets/img/filter.png';
import { Wrap, Img } from './styled';

interface IFilterSwitcher {
  onClick: () => void;
}

export const FilterSwitcher: FC<IFilterSwitcher> = ({ onClick }) => {
  return (
    <Wrap onClick={onClick}>
      <Img
        src={FilterUrl}
        alt="Moon"
      />
      Filters
    </Wrap>
  );
};
