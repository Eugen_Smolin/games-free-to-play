import styled from 'styled-components';

export const Wrap = styled.div`
  display: inline-flex;
  align-items: center;
  font-size: 20px;
  font-weight: 500;
  cursor: pointer;
`;

export const Img = styled.img`
  width: 20px;
  height: 20px;
  margin-right: 16px;
`;
