import { FC } from 'react';

import { TLocalCardGame } from 'pages/GamesListPage/store/types';
import { Typography } from 'components/AppTheme/GlobalStyled/components';
import { Card, ImgBox, Img, Box, Label, Desc } from './styled';

export const CardGame: FC<{ game: TLocalCardGame }> = ({ game }) => {
  const {
    id,
    thumbnail,
    title,
    platform,
    shortDescription,
    genre,
    releaseDate,
  } = game;

  return (
    <Card to={`/game/${id}`}>
      <ImgBox>
        <Img
          src={thumbnail}
          alt={title}
        />
      </ImgBox>
      <Box>
        <Typography
          fz="20px"
          fw={600}
        >
          {title}
        </Typography>
        <div>
          <Label>Platform:</Label>
          {platform}
        </div>
        <div>
          <Label>Genre:</Label>
          {genre}
        </div>
        <div>
          <Label>Release Date:</Label>
          {releaseDate}
        </div>
        <Desc>{shortDescription}</Desc>
      </Box>
    </Card>
  );
};
