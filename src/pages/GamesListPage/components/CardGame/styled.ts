import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ImgBg from 'assets/img/img-bg.png';

export const Card = styled(Link)`
  width: 100%;
  max-width: 380px;
  margin: 0 auto;
  border-radius: 12px;
  background-color: ${({ theme }) => theme.cardGame.bg};
  transition: 0.15s linear;

  &:hover {
    text-decoration: none;
    transform: scale(1.02);
  }
`;

export const ImgBox = styled.div`
  position: relative;
  display: block;
  width: 100%;
  padding-top: 56%;
  background: center center/30% no-repeat url(${ImgBg})
    ${({ theme }) => theme.cardGame.bg};
  border-radius: 12px 12px 0 0;
  overflow: hidden;
`;

export const Img = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
`;

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 8px;
  padding: 16px;
`;

export const Label = styled.div`
  display: inline-block;
  font-weight: 600;
  margin-right: 8px;
`;

export const Desc = styled.div`
  margin-top: 16px;
`;
