import {
  TApiCardGame,
  TApiParamsGames,
  TLocalCardGame,
  TLocalParamsGames,
} from 'pages/GamesListPage/store/types';

export const GameModel = (data: TApiCardGame): TLocalCardGame => ({
  id: data.id,
  developer: data.developer,
  freetogameProfileUrl: data.freetogame_profile_url,
  gameUrl: data.game_url,
  genre: data.genre,
  platform: data.platform,
  publisher: data.publisher,
  releaseDate: data.release_date,
  shortDescription: data.short_description,
  thumbnail: data.thumbnail,
  title: data.title,
});

export const GamesParamsModel = (data: TLocalParamsGames): TApiParamsGames => ({
  platform: data.platform,
  category: data.category,
  'sort-by': data.sortBy,
});
