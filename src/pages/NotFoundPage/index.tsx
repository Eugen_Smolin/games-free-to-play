import { Typography } from 'components/AppTheme/GlobalStyled/components';
import { Error } from './styled';

export const NotFoundPage = () => {
  return (
    <Error>
      <Typography fz="40px">404</Typography>
      <div>Not found page</div>
    </Error>
  );
};
