import {
  TApiGameDetails,
  TLocalGameDetails,
} from 'pages/GameDetailsPage/store/types';

export const GameDetailsModel = (data: TApiGameDetails): TLocalGameDetails => ({
  id: data.id,
  developer: data.developer,
  freetogameProfileUrl: data.freetogame_profile_url,
  gameUrl: data.game_url,
  genre: data.genre,
  platform: data.platform,
  publisher: data.publisher,
  releaseDate: data.release_date,
  shortDescription: data.short_description,
  thumbnail: data.thumbnail,
  title: data.title,
  status: data.status,
  description: data.description,
  screenshots: data.screenshots,
});
