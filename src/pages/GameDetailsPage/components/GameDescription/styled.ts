import styled from 'styled-components';

export const MainTitle = styled.div`
  font-size: 30px;
  font-weight: 600;
  margin-top: 8px;
  margin-bottom: 40px;
`;

export const Row = styled.div`
  margin-bottom: 12px;
`;

export const Title = styled.div`
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 16px;
`;

export const Desc = styled.div`
  margin-bottom: 40px;
`;

export const Label = styled.span`
  display: inline-block;
  font-weight: 600;
  margin-right: 8px;
`;
