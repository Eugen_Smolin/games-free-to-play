import { FC, useMemo } from 'react';

import { RBox } from 'ui/RBox';
import { ImagesGallery } from 'components/ImagesGallery';
import { TLocalGameDetails } from 'pages/GameDetailsPage/store/types';
import { MainTitle, Row, Title, Desc, Label } from './styled';

export const GameDescription: FC<TLocalGameDetails> = ({
  title,
  description,
  screenshots,
  developer,
  publisher,
  releaseDate,
}) => {
  const images = useMemo(() => {
    return screenshots.map((img) => ({
      original: img.image,
      thumbnail: img.image,
    }));
  }, [screenshots]);

  return (
    <RBox>
      <MainTitle>{title}</MainTitle>
      <Row>
        <Label>Developer:</Label>
        {developer}
      </Row>
      <Row>
        <Label>Publisher:</Label>
        {publisher}
      </Row>
      <Desc>
        <Label>Release Date:</Label>
        {releaseDate}
      </Desc>
      <Title>About</Title>
      <Desc>{description}</Desc>
      <Title>Screenshots</Title>
      {images.length ? <ImagesGallery images={images} /> : <div>Not found</div>}
    </RBox>
  );
};
