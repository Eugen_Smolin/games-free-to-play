import styled from 'styled-components';

export const Wrap = styled.div`
  position: relative;

  @media (max-width: 840px) {
    margin-bottom: 24px;
  }
`;

export const Sticky = styled.div`
  position: sticky;
  top: 24px;
`;

export const Img = styled.img`
  border-radius: 8px 8px 0 0;

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 12px;
  background: ${({ theme }) => theme.box.bg};
  padding: 16px;
  border-radius: 0 0 8px 8px;
`;

export const Link = styled.a`
  display: block;
  margin-left: auto;
`;

export const Label = styled.span`
  display: inline-block;
  font-weight: 600;
  margin-right: 8px;
`;
