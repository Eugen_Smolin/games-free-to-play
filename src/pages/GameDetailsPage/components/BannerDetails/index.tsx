import { FC } from 'react';

import { RButton } from 'ui/RButton';
import { TLocalGameDetails } from 'pages/GameDetailsPage/store/types';
import { Wrap, Sticky, Img, Box, Link, Label } from './styled';

export const BannerDetails: FC<TLocalGameDetails> = ({
  thumbnail,
  platform,
  status,
  genre,
  gameUrl,
}) => {
  return (
    <Wrap>
      <Sticky>
        <Img
          src={thumbnail}
          alt="Banner"
        />
        <Box>
          <div>
            <Label>Platform:</Label>
            {platform}
          </div>
          <div>
            <Label>Genre:</Label>
            {genre}
          </div>
          <div>
            <Label>Status:</Label>
            {status}
          </div>
          <Link
            href={gameUrl}
            target="_blank"
            rel="noreferrer"
          >
            <RButton>Play now</RButton>
          </Link>
        </Box>
      </Sticky>
    </Wrap>
  );
};
