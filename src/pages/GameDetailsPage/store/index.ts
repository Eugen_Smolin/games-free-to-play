import { createSlice } from '@reduxjs/toolkit';

import { selectors } from './selectors';
import { thunks } from './thunks';
import { StatusEnum } from 'types/enum';
import { GameDetailsModel } from 'pages/GameDetailsPage/models';
import { TGameDetailsSlice } from './types';

const initialState: TGameDetailsSlice = {
  data: null,
  error: {
    status: 404,
    message: 'The game ID is not valid',
  },
  fetchingStatus: StatusEnum.IDLE,
};

const slice = createSlice({
  name: 'gameDetails',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getGameDetailsThunk.pending, (state) => {
        state.fetchingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getGameDetailsThunk.fulfilled, (state, { payload }) => {
        if (payload) state.data = GameDetailsModel(payload);
        state.fetchingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getGameDetailsThunk.rejected, (state, { payload }) => {
        if (payload) state.error = payload;
        state.fetchingStatus = StatusEnum.FAIL;
      });
  },
});

export const gameDetailsSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export const gameDetailsReducer = slice.reducer;
