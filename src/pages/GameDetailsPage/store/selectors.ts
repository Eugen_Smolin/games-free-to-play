import { RootState } from 'store';

export const selectors = {
  data: (state: RootState) => state.gameDetailsReducer.data,
  error: (state: RootState) => state.gameDetailsReducer.error,
  fetchingStatus: (state: RootState) => state.gameDetailsReducer.fetchingStatus,
};
