import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';

import { http } from 'api/http';
import { WEB_API_ROUTES } from 'api/api-routes';
import { TApiGameDetails, TAxiosDataError, TThunkAPI } from './types';

const getGameDetailsThunk = createAsyncThunk<
  TApiGameDetails,
  string,
  TThunkAPI
>('gameDetailsReducer/getGameDetailsThunk', async (id, { rejectWithValue }) => {
  try {
    const { data } = await http.get(WEB_API_ROUTES.getGameDetails, {
      params: { id },
    });
    return data;
  } catch (err) {
    if (axios.isAxiosError(err)) {
      const { status, data } = err.response as AxiosResponse;
      const dataError = data as TAxiosDataError;
      return rejectWithValue({ status, message: dataError.status_message });
    }

    throw err;
  }
});

export const thunks = {
  getGameDetailsThunk,
};
