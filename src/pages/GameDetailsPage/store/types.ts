import { StatusEnum } from 'types/enum';

export type TApiScreenshot = {
  id: number;
  image: string;
};

export type TLocalGameDetails = {
  id: number;
  developer: string;
  freetogameProfileUrl: string;
  gameUrl: string;
  genre: string;
  platform: string;
  publisher: string;
  releaseDate: string;
  shortDescription: string;
  thumbnail: string;
  title: string;
  description: string;
  status: string;
  screenshots: TApiScreenshot[];
};

export type TApiGameDetails = {
  id: number;
  developer: string;
  freetogame_profile_url: string;
  game_url: string;
  genre: string;
  platform: string;
  publisher: string;
  release_date: string;
  short_description: string;
  thumbnail: string;
  title: string;
  description: string;
  status: string;
  screenshots: TApiScreenshot[];
};

export type TGameDetailsSlice = {
  data: TLocalGameDetails | null;
  error: TAxiosReturnError;
  fetchingStatus: StatusEnum;
};

export type TAxiosDataError = {
  status: number;
  status_message: string;
};

export type TAxiosReturnError = {
  status: number;
  message: string;
};

export type TThunkAPI = {
  rejectValue: TAxiosReturnError;
};
