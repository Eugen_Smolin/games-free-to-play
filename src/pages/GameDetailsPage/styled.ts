import styled from 'styled-components';

export const Wrap = styled.div`
  display: grid;
  grid-template-columns: 365px 1fr;
  grid-column-gap: 24px;
  margin-bottom: 24px;

  @media (max-width: 840px) {
    grid-template-columns: 1fr;
  }
`;
