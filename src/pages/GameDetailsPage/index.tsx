import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { RLoader } from 'ui/RLoader';
import { ErrorServer } from 'components/ErrorServer';
import { BannerDetails } from './components/BannerDetails';
import { GameDescription } from './components/GameDescription';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { gameDetailsSlice } from './store';
import { StatusEnum } from 'types/enum';
import { Wrap } from './styled';

export const GameDetailsPage = () => {
  const { gameId } = useParams();
  const dispatch = useAppDispatch();
  const status = useAppSelector(gameDetailsSlice.selectors.fetchingStatus);
  const details = useAppSelector(gameDetailsSlice.selectors.data);
  const error = useAppSelector(gameDetailsSlice.selectors.error);

  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(gameDetailsSlice.thunks.getGameDetailsThunk(gameId || '1'));
  }, []);

  if (status === StatusEnum.PENDING) {
    return <RLoader />;
  }

  if (status === StatusEnum.FAIL) {
    return (
      <ErrorServer
        status={error.status}
        message={error.message}
      />
    );
  }

  if (!details) return null;

  return (
    <Wrap>
      <BannerDetails {...details} />
      <GameDescription {...details} />
    </Wrap>
  );
};
