import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import { store } from 'store';
import { App } from 'App';
import { Theme } from 'components/AppTheme';
import { GlobalStyles } from 'components/AppTheme/GlobalStyled';
import { Router } from './router';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <Theme>
        <GlobalStyles />
        <Router>
          <App />
        </Router>
      </Theme>
    </Provider>
  </React.StrictMode>
);
