import { configureStore, combineReducers } from '@reduxjs/toolkit';

import { gamesListReducer } from 'pages/GamesListPage/store';
import { gameDetailsReducer } from 'pages/GameDetailsPage/store';

const rootReducer = combineReducers({
  gamesListReducer,
  gameDetailsReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
