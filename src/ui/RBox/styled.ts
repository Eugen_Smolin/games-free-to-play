import styled from 'styled-components';
import { CSSProperties } from 'react';

interface BoxProps {
  css: CSSProperties;
}

export const Box = styled.div<BoxProps>`
  position: relative;
  background: ${({ theme }) => theme.box.bg};
  padding: 16px;
  border-radius: 8px;

  ${({ css }) => css};
`;
