import styled from 'styled-components';

export const Button = styled.button`
  font-size: 14px;
  font-weight: 600;
  border: none;
  color: ${({ theme }) => theme.button.color};
  background: ${({ theme }) => theme.button.bg};
  text-transform: uppercase;
  border-radius: 100px;
  padding: 10px 24px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.button.hover};
  }
`;
