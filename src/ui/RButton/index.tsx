import { ButtonHTMLAttributes, FC, ReactNode } from 'react';

import { Button } from './styled';

interface RButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: () => void;
  children: ReactNode;
}

export const RButton: FC<RButtonProps> = ({ onClick, children }) => {
  return <Button onClick={onClick}>{children}</Button>;
};
