import styled from 'styled-components';

export const Wrap = styled.div`
  padding: 24px 0;

  .rc-pagination {
    display: flex;
    align-items: center;
    justify-content: center;
    column-gap: 8px;
    list-style: none;
  }

  .rc-pagination-prev,
  .rc-pagination-next,
  .rc-pagination-jump-prev,
  .rc-pagination-jump-next,
  .rc-pagination-item {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 36px;
    height: 36px;
    color: ${({ theme }) => theme.pag.color};
    background-color: ${({ theme }) => theme.pag.bg};
    border-radius: 4px;
    font-weight: 500;
    font-size: 16px;
    user-select: none;
    cursor: pointer;

    @media (max-width: 500px) {
      width: 32px;
      height: 32px;
      font-size: 14px;
    }

    &.rc-pagination-item-active,
    &:hover {
      background-color: ${({ theme }) => theme.pag.hover};
    }

    &.rc-pagination-disabled {
      opacity: 0.5;
      cursor: not-allowed;
    }
  }
`;
