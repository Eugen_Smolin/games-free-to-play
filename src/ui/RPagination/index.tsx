import { FC } from 'react';
import Pagination from 'rc-pagination';

import { Wrap } from './styled';

interface RPaginationProps {
  page: number;
  perPage: number;
  total: number;
  onChangePage: (page: number) => void;
}

export const RPagination: FC<RPaginationProps> = ({
  page,
  perPage,
  total,
  onChangePage,
}) => {
  const onChange = (page: number) => {
    onChangePage(page);
  };

  return (
    <Wrap>
      <Pagination
        current={page}
        pageSize={perPage}
        total={total}
        showLessItems
        onChange={onChange}
        jumpPrevIcon="..."
        jumpNextIcon="..."
        prevIcon="<"
        nextIcon=">"
      />
    </Wrap>
  );
};
