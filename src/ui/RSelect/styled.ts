import styled, { css } from 'styled-components';

import { scrollBar } from 'components/AppTheme/GlobalStyled';

export const Select = styled.div`
  position: relative;
  max-width: 370px;
  width: 100%;
  height: 56px;
`;

export const Field = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  border: 1px solid ${({ theme }) => theme.select.border};
  border-radius: 4px;
  background-color: ${({ theme }) => theme.select.bg};
  padding: 4px 16px;
  cursor: pointer;
`;

export const FieldText = styled.div`
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: ${({ theme }) => theme.select.color};
  text-transform: capitalize;
`;

export const FieldIcon = styled.div<{ isActive: boolean }>`
  display: flex;
  ${({ isActive }) =>
    isActive &&
    css`
      transform: rotate(180deg);
    `};

  & > svg path {
    fill: ${({ theme }) => theme.select.color};
  }
`;

export const Dropdown = styled.div`
  position: absolute;
  top: 100%;
  width: 100%;
  max-height: 232px;
  padding: 8px 0;
  background-color: ${({ theme }) => theme.select.dropdown.bg};
  border-radius: 4px;
  overflow-y: auto;
  z-index: 10;
  ${scrollBar};
`;

export const Item = styled.div<{ isActive: boolean }>`
  display: flex;
  align-items: center;
  height: 56px;
  padding: 8px 12px;
  font-size: 16px;
  text-transform: capitalize;
  cursor: pointer;
  transition: 0.15s ease;

  ${({ isActive }) =>
    isActive &&
    css`
      background-color: ${({ theme }) => theme.select.dropdown.hover};
    `};

  &:hover {
    background-color: ${({ theme }) => theme.select.dropdown.hover};
  }
`;
