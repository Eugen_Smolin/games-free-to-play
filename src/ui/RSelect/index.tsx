import { useMemo, useRef, useState } from 'react';

import { ReactComponent as IconArrowSvg } from 'assets/img/icon-arrow.svg';
import { useOutsideClick } from 'hooks/useOutsideClick';
import { Select, Field, FieldText, FieldIcon, Dropdown, Item } from './styled';

interface IOptions {
  value: string;
  label: string;
}

interface RSelectProps {
  value: string;
  onChange: (value: string) => void;
  options: IOptions[];
}

export const RSelect = ({
  value,
  onChange = () => {},
  options = [],
}: RSelectProps) => {
  const [isVisible, setVisible] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const onSelect = (val: string) => {
    onChange(val);
    setVisible(false);
  };

  useOutsideClick(dropdownRef, () => {
    if (isVisible) setVisible(false);
  });

  const labelName = useMemo(() => {
    const current = options.find((option) => option.value === value);
    return current?.label;
  }, [value]);

  return (
    <Select>
      <Field onClick={() => setVisible(true)}>
        <FieldText>{labelName}</FieldText>
        <FieldIcon isActive={isVisible}>
          <IconArrowSvg />
        </FieldIcon>
      </Field>
      {isVisible && (
        <Dropdown ref={dropdownRef}>
          {options.map((option) => (
            <Item
              key={option.value}
              isActive={option.value === value}
              onClick={() => onSelect(option.value)}
            >
              {option.label}
            </Item>
          ))}
        </Dropdown>
      )}
    </Select>
  );
};
