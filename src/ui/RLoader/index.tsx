import LoaderUrl from 'assets/img/loader.svg';
import { Loader, Img, Text } from './styled';

export const RLoader = () => {
  return (
    <Loader>
      <Img
        src={LoaderUrl}
        alt=""
      />
      <Text>Loading...</Text>
    </Loader>
  );
};
