import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  0% {
    transform: rotate(0);
  }
  50% {
    transform: rotate(360deg);
  }
  100% {
    transform: rotate(0);
  }
`;

export const Loader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const Img = styled.img`
  width: 36px;
  height: 36px;
  animation: 1.2s ${rotate} ease-out infinite;
  margin-right: 8px;
`;

export const Text = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 40px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.primary};
`;
