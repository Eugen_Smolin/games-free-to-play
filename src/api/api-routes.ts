export const WEB_API_ROUTES = {
  getListOfGames: '/games',
  getGameDetails: '/game',
};
