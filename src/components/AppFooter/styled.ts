import styled from 'styled-components';

export const Wrap = styled.footer`
  background: ${({ theme }) => theme.box.bg};
  padding: 16px;
  margin-top: auto;
  text-align: center;
`;

export const Links = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  column-gap: 40px;
  margin-bottom: 16px;
`;

export const Link = styled.a`
  color: ${({ theme }) => theme.primary};
`;
