import { Wrap, Links, Link } from './styled';

const currentYear = new Date().getFullYear();

const linksProfiles = [
  {
    title: 'Linkedin profile',
    link: 'https://www.linkedin.com/in/eugene-smolin',
  },
  {
    title: 'Source code',
    link: 'https://gitlab.com/Eugen_Smolin/games-free-to-play',
  },
  {
    title: 'About me',
    link: 'https://es-frontend.vercel.com',
  },
];

export const Footer = () => {
  return (
    <Wrap>
      <Links>
        {linksProfiles.map((item) => (
          <Link
            key={item.title}
            href={item.link}
            target="_blank"
            rel="noreferrer"
          >
            {item.title}
          </Link>
        ))}
      </Links>
      Copyright &copy; {currentYear} Eugene Smolin
    </Wrap>
  );
};
