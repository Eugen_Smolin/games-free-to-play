import { FC } from 'react';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';

import { Wrap } from './styled';

interface IImages {
  original: string;
  thumbnail: string;
}

interface IImageGalleryProps {
  images: IImages[];
}

export const ImagesGallery: FC<IImageGalleryProps> = ({ images }) => {
  return (
    <Wrap>
      <ImageGallery
        items={images}
        showPlayButton={false}
        showNav={false}
      />
    </Wrap>
  );
};
