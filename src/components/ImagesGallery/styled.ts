import styled from 'styled-components';

export const Wrap = styled.div`
  .image-gallery-thumbnail.active,
  .image-gallery-thumbnail:focus,
  .image-gallery-thumbnail:hover {
    border-color: ${({ theme }) => theme.primary};
  }
`;
