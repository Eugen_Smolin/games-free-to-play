import styled from 'styled-components';

type TTypography = {
  display?: string;
  fz?: string;
  fw?: number;
  textPos?: string;
};

export const Typography = styled.div<TTypography>`
  display: ${({ display = 'block' }) => display};
  font-size: ${({ fz = '14px' }) => fz};
  font-style: normal;
  font-weight: ${({ fw = 400 }) => fw};
  text-align: ${({ textPos = 'start' }) => textPos};
  line-height: 142%;
`;
