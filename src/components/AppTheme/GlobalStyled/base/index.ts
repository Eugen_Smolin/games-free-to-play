import { css } from 'styled-components';

export const Base = css`
  body {
    font-family: 'Raleway', sans-serif;
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 1.4;
    color: ${({ theme }) => theme.app.color};
    background-color: ${({ theme }) => theme.app.bg};
  }
`;
