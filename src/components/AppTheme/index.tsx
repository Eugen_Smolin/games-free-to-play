import React, { FC, ReactNode } from 'react';
import { ThemeProvider } from 'styled-components';

const theme = {
  primary: '#D0BCFF',
  app: {
    bg: '#141218',
    color: '#E6E0E9',
  },
  header: {
    bg: '#211F26',
  },
  button: {
    bg: '#D0BCFF',
    color: '#381E72',
    hover: '#b4a2dd',
  },
  select: {
    color: '#E6E0E9',
    label: '#CAC4D0',
    border: '#938F99',
    focus: '#D0BCFF',
    bg: '#141218',
    dropdown: {
      bg: '#211F26',
      hover: '#36343B',
    },
  },
  box: {
    bg: '#211F26',
  },
  cardGame: {
    bg: '#1D1B20',
  },
  pag: {
    color: '#E6E0E9',
    bg: '#211F26',
    hover: '#36343B',
  },
};

interface ThemeProps {
  children: ReactNode;
}

export const Theme: FC<ThemeProps> = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export type CustomTheme = typeof theme;
