import { FC } from 'react';

import { Error } from './styled';

interface IErrorServer {
  status: number;
  message: string;
}

export const ErrorServer: FC<IErrorServer> = ({ status, message }) => {
  return (
    <Error>
      <div>Status {status}:</div>
      <div>{message || 'Request was failed!'}</div>
    </Error>
  );
};
