import LogoUrl from 'assets/img/logo.png';
import { Img } from './styled';

export const Logo = () => {
  return (
    <Img
      src={LogoUrl}
      alt="Gamepad"
    />
  );
};
