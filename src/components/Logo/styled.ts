import styled from 'styled-components';

export const Img = styled.img`
  display: block;
  width: 60px;
  height: 60px;
`;
