import { Logo } from 'components/Logo';
import { Header, Content, LinkToHome, Text } from './styled';

export const AppHeader = () => {
  return (
    <Header>
      <Content>
        <LinkToHome to="/">
          <Logo />
          <Text>FreeGames</Text>
        </LinkToHome>
      </Content>
    </Header>
  );
};
