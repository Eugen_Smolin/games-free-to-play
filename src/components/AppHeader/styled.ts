import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { container } from 'components/AppTheme/GlobalStyled';

export const Header = styled.header`
  height: 60px;
  margin-bottom: 24px;
  background-color: ${({ theme }) => theme.header.bg};
`;

export const Content = styled.div`
  ${container};
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

export const LinkToHome = styled(Link)`
  display: flex;
  align-items: center;
  column-gap: 8px;

  &:hover {
    text-decoration: none;
  }
`;

export const Text = styled.div`
  padding: 0 2px;
  font-size: 24px;
  font-weight: 500;
  background: linear-gradient(to right, #de00ff, #8200ff);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;
