export const arrayChunks = <T>(list: T[] = [], size = 5): T[][] => {
  const chunkArr: T[][] = [];

  for (let i = 0; i < list.length; i += size) {
    const chunk = list.slice(i, i + size);
    chunkArr.push(chunk);
  }

  return chunkArr;
};
