type TEnteringObject<T> = Record<keyof T, string>;

export const extractAllFromObject = <T>(
  data: TEnteringObject<T>
): TEnteringObject<T> => {
  const copiedData = { ...data };

  for (const key in copiedData) {
    if (!copiedData[key] || copiedData[key] === 'all') delete copiedData[key];
  }

  return copiedData;
};
