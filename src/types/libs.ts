export type TPagination = {
  page: number;
  perPage: number;
  total: number;
};
