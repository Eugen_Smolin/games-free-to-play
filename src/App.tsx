import { AppLayout } from 'layouts/AppLayout';

export const App = () => {
  return <AppLayout />;
};
